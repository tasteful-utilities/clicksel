package org.tastefuljava.clicksel;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ListenerList {
    private static final Logger LOG
            = Logger.getLogger(ListenerList.class.getName());

    private final List<Object> list = new ArrayList<>();

    public void addListener(Object listener) {
        list.add(listener);
    }

    public void removeListener(Object listener) {
        list.remove(listener);
    }

    public <T> T getNotifier(Class<T> intf) {
        ClassLoader cl = intf.getClassLoader();
        Object proxy = Proxy.newProxyInstance(cl, new Class[] {intf},
                (pxy, method, args) -> {
                    Object result = null;
                    for (Object listener: list) {
                        try {
                            result = method.invoke(listener, args);
                        }catch (IllegalAccessException
                                | InvocationTargetException ex) {
                            LOG.log(Level.SEVERE, null, ex);
                        }
                    }
                    return result;
                });
        return intf.cast(proxy);
    }

    public Object notify(String name, Object... args) {
        Object result = null;
        for (Object listener: list) {
            Method method = findMethod(listener, name, args);
            if (method != null) {
                try {
                    result = method.invoke(listener, args);
                } catch (IllegalAccessException | IllegalArgumentException
                        | InvocationTargetException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            }
        }
        return result;
    }

    private Method findMethod(Object instance, String name, Object args[]) {
        Method methods[] = instance.getClass().getMethods();
        for (Method method: methods) {
            if (name.equals(method.getName())) {
                Class formalTypes[] = method.getParameterTypes();
                if (args == null) {
                    if (formalTypes.length == 0) {
                        return method;
                    }
                } else if (args.length == formalTypes.length) {
                    return method;
                }
            }
        }
        return null;
    }
}
