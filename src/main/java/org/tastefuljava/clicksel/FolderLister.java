package org.tastefuljava.clicksel;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class FolderLister {
    private static final ExecutorService XTOR = Executors.newSingleThreadExecutor();

    public static void listFolder(File folder, Predicate<String> search,
            Consumer<FileInfo> handler) {
        XTOR.submit(() -> {
            folder.listFiles(new Filter(handler, search));
            handler.accept(null);
        });
    }

    private static class Filter implements FileFilter {
        private final Consumer<FileInfo> handler;
        private final Predicate<String> pred;

        public Filter(Consumer<FileInfo> handler, Predicate<String> search) {
            this.handler = handler;
            this.pred = search;
        }

        @Override
        public boolean accept(File file) {
            if (!file.getName().startsWith(".")) {
                if (file.isDirectory()) {
                    file.listFiles(this);
                } else if (file.isFile() && pred.test(file.getName())) {
                    handler.accept(FileInfo.fromFile(file));
                }
            }
            return false;
        }
    }
}
