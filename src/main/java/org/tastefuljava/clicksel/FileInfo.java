package org.tastefuljava.clicksel;

import java.io.File;

public class FileInfo {
    private final String name;
    private final String folder;
    private final long size;

    public static FileInfo fromFile(File file) {
        return new FileInfo(file.getName(), file.getParent(), file.length());
    }

    public FileInfo(String name, String folder, long size) {
        this.name = name;
        this.folder = folder;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public String getFolder() {
        return folder;
    }

    public long getSize() {
        return size;
    }

    public File toFile() {
        return new File(folder, name);
    }

    @Override
    public String toString() {
        return name + " (" + size + ')';
    }
}
