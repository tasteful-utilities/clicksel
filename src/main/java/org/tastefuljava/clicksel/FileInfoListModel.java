package org.tastefuljava.clicksel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class FileInfoListModel implements ListModel<FileInfo> {
    private final List<FileInfo> list = new ArrayList<>();
    private final ListenerList listeners = new ListenerList();
    private final ListDataListener notifier
            = listeners.getNotifier(ListDataListener.class);

    private Comparator<FileInfo> order = FileInfoComparator.BY_NAME;

    @Override
    public int getSize() {
        return list.size();
    }

    @Override
    public FileInfo getElementAt(int index) {
        return list.get(index);
    }

    public Comparator<FileInfo> getOrder() {
        return order;
    }

    public void setOrder(Comparator<FileInfo> order) {
        this.order = order;
        Collections.sort(list, order);
        int end = list.size();
        notifier.intervalRemoved(new ListDataEvent(
                this, ListDataEvent.INTERVAL_REMOVED, 0, end));
        notifier.intervalAdded(new ListDataEvent(
                this, ListDataEvent.INTERVAL_ADDED, 0, end-1));
    }

    public void add(FileInfo fi) {
        int ix = Collections.binarySearch(list, fi, order);
        if (ix < 0) {
            ix = -ix-1;
        }
        list.add(ix, fi);
        notifier.intervalAdded(new ListDataEvent(
                this, ListDataEvent.INTERVAL_ADDED, ix, ix));
    }

    public FileInfo remove(int ix) {
        FileInfo result = list.remove(ix);
        notifier.intervalRemoved(new ListDataEvent(
                this, ListDataEvent.INTERVAL_REMOVED, ix, ix));
        return result;
    }

    public boolean remove(FileInfo fi) {
        int ix = list.indexOf(fi);
        if (ix < 0) {
            return false;
        }
        remove(ix);
        return true;
    }

    public void clear() {
        int end = list.size();
        list.clear();
        notifier.intervalRemoved(new ListDataEvent(
                this, ListDataEvent.INTERVAL_REMOVED, 0, end));
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.addListener(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.removeListener(l);
    }
}
