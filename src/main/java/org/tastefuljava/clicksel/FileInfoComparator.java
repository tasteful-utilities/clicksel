package org.tastefuljava.clicksel;

import java.util.Comparator;

public enum FileInfoComparator implements Comparator<FileInfo> {
    BY_NAME {
        @Override
        public int compare(FileInfo a, FileInfo b) {
            int r = String.CASE_INSENSITIVE_ORDER.compare(
                    a.getName(), b.getName());
            if (r != 0) {
                return r;
            }
            return a.getFolder().compareTo(b.getFolder());
        }

        @Override
        public String toString() {
            return "By name";
        }
    },

    BY_SIZE {
        @Override
        public int compare(FileInfo a, FileInfo b) {
            if (a.getSize() > b.getSize()) {
                return 1;
            } else if (a.getSize() < b.getSize()) {
                return -1;
            }
            return BY_NAME.compare(a, b);
        }

        @Override
        public String toString() {
            return "By size";
        }
    };
}
