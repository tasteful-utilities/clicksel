package org.tastefuljava.clicksel;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileTransferable implements Transferable {
    private static final DataFlavor[] FLAVORS = {DataFlavor.javaFileListFlavor};

    private final List<FileInfo> files = new ArrayList<>();

    public FileTransferable(List<FileInfo> files) {
        this.files.addAll(files);
    }

    public List<FileInfo> getFiles() {
        return files;
    }

    @Override
    public DataFlavor[] getTransferDataFlavors() {
        return FLAVORS;
    }

    @Override
    public boolean isDataFlavorSupported(DataFlavor flavor) {
        for (DataFlavor f: FLAVORS) {
            if (f.equals(flavor)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Object getTransferData(DataFlavor flavor)
            throws UnsupportedFlavorException, IOException {
        if (flavor.equals(DataFlavor.javaFileListFlavor)) {
            List<File> result = new ArrayList<>();
            files.forEach(fi->result.add(fi.toFile()));
            return result;
        } else {
            throw new UnsupportedFlavorException(flavor);
        }
    }
}
