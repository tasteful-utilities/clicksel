package org.tastefuljava.clicksel;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.datatransfer.Transferable;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileFilter;

public class MainDialog extends javax.swing.JDialog {
    private static final Logger LOG = Logger.getLogger(
            MainDialog.class.getName());
    private static final Predicate<String> PRED_VIDEOFILE
            = Pattern.compile(
                    ".*\\.(mp4|mkv|m4v|mpg|avi|wmv)",
                    Pattern.CASE_INSENSITIVE)
                    .asMatchPredicate();

    private static final FileFilter VIDEO_FILTER = new FileFilter() {
        @Override
        public boolean accept(File f) {
            return f.isDirectory()
                    || PRED_VIDEOFILE.test(f.getName());
        }

        @Override
        public String getDescription() {
            return "Video files (*.mp4,*.mkv,*.m4v,*.mpg)";
        }
    };

    private final FileInfoListModel listModel = new FileInfoListModel();

    public MainDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        list.addListSelectionListener((ListSelectionEvent e) -> {
            selectionChanged();
        });
        list.setDragEnabled(true);
        list.setTransferHandler(new TransferHandler() {
            @Override
            protected Transferable createTransferable(JComponent c) {
                List<FileInfo> files = list.getSelectedValuesList();
                return new FileTransferable(files);
            }

            @Override
            public int getSourceActions(JComponent c) {
                return TransferHandler.MOVE;
            }

            @Override
            protected void exportDone(JComponent source, Transferable data,
                    int action) {
                if (action == TransferHandler.MOVE
                        && data instanceof FileTransferable) {
                    FileTransferable ft = (FileTransferable)data;
                    for (FileInfo fi: ft.getFiles()) {
                        listModel.remove(fi);
                    }
                    list.clearSelection();
                }
            }
        });
        chooser.addChoosableFileFilter(VIDEO_FILTER);
        open.setEnabled(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        chooser = new javax.swing.JFileChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        list = new javax.swing.JList<>();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        search = new javax.swing.JTextField();
        chooseFolder = new javax.swing.JButton();
        order = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        folderName = new javax.swing.JTextField();
        status = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        open = new javax.swing.JButton();
        delete = new javax.swing.JButton();

        chooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        chooser.setFileSelectionMode(javax.swing.JFileChooser.DIRECTORIES_ONLY);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jScrollPane1.setHorizontalScrollBarPolicy(javax.swing.ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane1.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

        list.setModel(listModel);
        list.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                listMouseMoved(evt);
            }
        });
        list.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(list);

        getContentPane().add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel1.setLayout(new java.awt.GridBagLayout());

        jLabel1.setText("Search:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 5);
        jPanel1.add(jLabel1, gridBagConstraints);

        search.setMinimumSize(new java.awt.Dimension(120, 26));
        search.setPreferredSize(new java.awt.Dimension(120, 26));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        jPanel1.add(search, gridBagConstraints);

        chooseFolder.setText("Choose...");
        chooseFolder.setToolTipText("");
        chooseFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chooseFolderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        jPanel1.add(chooseFolder, gridBagConstraints);

        order.setModel(new DefaultComboBoxModel<FileInfoComparator>(FileInfoComparator.values()));
        order.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                orderActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        jPanel1.add(order, gridBagConstraints);

        getContentPane().add(jPanel1, java.awt.BorderLayout.NORTH);

        jPanel2.setLayout(new java.awt.BorderLayout());

        folderName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jPanel2.add(folderName, java.awt.BorderLayout.PAGE_START);

        status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        status.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jPanel2.add(status, java.awt.BorderLayout.CENTER);

        jPanel3.setLayout(new java.awt.GridBagLayout());

        open.setText("Open...");
        open.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 12, 0, 5);
        jPanel3.add(open, gridBagConstraints);

        delete.setText("Delete");
        delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 11);
        jPanel3.add(delete, gridBagConstraints);

        jPanel2.add(jPanel3, java.awt.BorderLayout.PAGE_END);

        getContentPane().add(jPanel2, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void chooseFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chooseFolderActionPerformed
        int r = chooser.showOpenDialog(this);
        if (r == JFileChooser.APPROVE_OPTION) {
            File folder = chooser.getSelectedFile();
            status.setText("listing...");
            listModel.clear();
            String searchStr = search.getText().toLowerCase();
            Predicate<String> pred = PRED_VIDEOFILE.and(
                    name->name.toLowerCase().contains(searchStr));
            FolderLister.listFolder(folder, pred, fi -> {
                SwingUtilities.invokeLater(() -> {
                    if (fi == null) {
                        status.setText("done");
                    } else {
                        listModel.add(fi);
                    }
                });
            });
        }
    }//GEN-LAST:event_chooseFolderActionPerformed

    private void deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteActionPerformed
        int[] indices = list.getSelectedIndices();
        if (indices != null && indices.length > 0) {
            for (int j = indices.length; --j >= 0; ) {
                int ix = indices[j];
                FileInfo fi = listModel.getElementAt(ix);
                File file = fi.toFile();
                if (file.delete()) {
                    listModel.remove(ix);
                }
            }
            list.clearSelection();
        }
    }//GEN-LAST:event_deleteActionPerformed

    private void openActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openActionPerformed
        FileInfo fi = list.getSelectedValue();
        openFile(fi);
    }//GEN-LAST:event_openActionPerformed

    private void openFile(FileInfo fi) {
        if (Desktop.isDesktopSupported()) {
            try {
                Desktop.getDesktop().open(fi.toFile());
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
    }

    private void listMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listMouseClicked
        if (evt.getClickCount() == 2) {
            int ix = list.locationToIndex(evt.getPoint());
            if (ix >= 0 && ix < listModel.getSize()) {
                FileInfo fi = listModel.getElementAt(ix);
                openFile(fi);
            }
        }
    }//GEN-LAST:event_listMouseClicked

    private void listMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listMouseMoved
        int ix = list.locationToIndex(evt.getPoint());
        if(ix >= 0) {
            FileInfo fi = listModel.getElementAt(ix);
            if (fi != null) {
                list.setToolTipText(fi.getFolder());
            }
        }
    }//GEN-LAST:event_listMouseMoved

    private void orderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_orderActionPerformed
        listModel.setOrder((Comparator<FileInfo>) order.getSelectedItem());
    }//GEN-LAST:event_orderActionPerformed

    private void selectionChanged() {
        int ix = list.getAnchorSelectionIndex();
        if (ix >= 0 && ix < listModel.getSize()) {
            FileInfo fi = listModel.getElementAt(ix);
            folderName.setText(fi.getFolder());
        }
        open.setEnabled(Desktop.isDesktopSupported()
                && list.getSelectedIndex() >= 0);
    }

    public static void main(String args[]) {
        try {
            for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException
                | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            LOG.log(java.util.logging.Level.SEVERE, null, ex);
        }

        EventQueue.invokeLater(() -> {
            MainDialog dialog = new MainDialog(new JFrame(), true);
            dialog.addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
            dialog.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton chooseFolder;
    private javax.swing.JFileChooser chooser;
    private javax.swing.JButton delete;
    private javax.swing.JTextField folderName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<FileInfo> list;
    private javax.swing.JButton open;
    private javax.swing.JComboBox<FileInfoComparator> order;
    private javax.swing.JTextField search;
    private javax.swing.JLabel status;
    // End of variables declaration//GEN-END:variables
}
